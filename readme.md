# Clustering techniques

## How can you start the project?
1. Download the project <b>clustering_techniques</b> 
2. Open terminal or cmd and type <b>cd clustering_techniques</b>
3. You have to have Anaconda is installed on you computer: https://www.anaconda.com/
4. Create the environment:
   Type conda create -f clustering_techniques.yml in terminal or cmd.
3. Start the envornment with the command: <br>
    For Mac users: <b>source activate clustering_techniques</b><br>
    For Windows users: <b>activate clustering_techniques</b> <br>

    If you want to deactivate the command you can type this in the terminal or cmd.
    For Mac users: <b>source deactivate clustering_techniques</b><br>
    For Windows users: <b>deactivate clustering_techniques<b><br>
4. Start the experiment with the following command:
    <b>python main.pyc</b> 
    
The code uploaded is a binary version. If you want to look at the source code, please write me an <b>email: mangelova@tu-plovdiv.bg</b>.

<b>LNAI-2019 branch contains extended version.</b>
